This wmiirc script does a few things:

* Shows a fancy status bar that looks like this:

 CPUTemp: 40 32 37 36 | CPUMHz: 2600 1600 1600 1600 | Usage: / 3%, /home 24%, /mount/big 79% | Volume: ########...(78%) | RAM: ##.........(6.6G) | Connected to: <my wifi network> | Sat Apr 05 18:39

* It automatically fetches a new wallpaper at every session from reddit's /r/wallpapers subreddit (top of the week) (This often fails due to reddit servers being busy, if it fails, run manually using shortcut described below)

* It provides a few new shortcuts.

MODKEY is set to Windows key.

MODKEY-Control-space: cycles through keyboard layouts.
MODKEY-Shift-w      : Changes wallpaper
CTRL-ALT-DELETE	    : Calls xscreensaver-command -lock
CTRL-ALT-Q	    : Asks to reboot with gksudo

* It provides support for keyboard's volume up and down keys.

* It gives wmii hacker colors (green on black)

Configuration is not really streamlined so you should read this if you want to use it:

It has been tested on Ubuntu Trusty (14.04) with the wmii package available in Ubuntu repositories.


INSTALLATION:

Install requirements:
sudo apt-get install feh lm-sensors

! Warning, the following may overwrite your .wmiirc_local file in ~/.wmii/

$ cd ...somewhere...
$ git clone git@bitbucket.org:benoitcsirois/bens_wmii_rc.git
$ cd bens_wmii_rc/
$ ln -sr wmiirc_local ~/.wmii/
$ ln -sr reddit_wallpaper_getter.py ~/.wmii/
$ ln -sr chkb.py ~/.wmii/

1. Status bar:

* Edit wmiirc_local, at the bottom, edit status() command, change the Usage: section to whateveer partitions you want to monitor.
* Also change the wlan interface to whatever interface you want to monitor (by default it's wlan0)

2. Keyboard layouts:

* Edit chkb.py, change AVAILABLE_LAYOUTS to whatever layouts you want to have access to in wmii.


UBUNTU TRUSTY THAR (14.04) IMPORTANT NOTES!

If you're emacs user, or want to use some of the keyboard shortcuts defined above, you *will* need to run ibus-setup (Perhaps sudo ibus-setup but not sure), and set the "Next Keyboard Layout" command to nothing because it will prevent from using Modkey-Space.
