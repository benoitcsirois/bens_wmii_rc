#!/usr/bin/python
#
# This script does 2 things:
# When given the "show" command, it will output the current keyboard layout.
# When given the "cycle" command, it will cycle the keyboard from the AVAILABLE_LAYOUTS
# list.

import subprocess
import re

AVAILABLE_LAYOUTS = ['us', 'ca(multix)']
KBRE = re.compile('^layout: *[()\w]*$', flags=re.MULTILINE)

def check_output(command):
    process = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, universal_newlines=True)
    output = process.communicate()
    retcode = process.poll()
    if retcode:
            raise subprocess.CalledProcessError(retcode, command, output=output[0])
    return output 


def get_current_layout():
    r1, _ = check_output('setxkbmap -query')
    return KBRE.findall(r1)[0].split(' ')[-1:][0]

def cycle_layout():
    next_index = AVAILABLE_LAYOUTS.index(get_current_layout())+1
    if len(AVAILABLE_LAYOUTS) - 1 < next_index:
        next_index = 0
    check_output('setxkbmap \'%s\'' % AVAILABLE_LAYOUTS[next_index])

import argparse
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Prints out current keyboard layout, or cycles through available layouts.')
    parser.add_argument('command', choices=['cycle', 'show'], help='cycle: Cycles through available layouts, show: Shows current layout')
    args = parser.parse_args()
    if args:
        if args.command == 'cycle':
            cycle_layout()
            print get_current_layout()
        if args.command == 'show':
            print get_current_layout()
